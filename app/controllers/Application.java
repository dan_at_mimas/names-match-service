package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

import org.codehaus.jackson.node.ObjectNode;
import org.codehaus.jackson.JsonNode;
import play.mvc.BodyParser;
import play.libs.Json;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import uk.ac.mimas.names.disambiguator.*;
import uk.ac.mimas.names.disambiguator.types.*;
import uk.ac.mimas.names.disambiguator.util.*;

public class Application extends Controller {
  
    public static Result index() {
        return ok(index.render());
    }

    @BodyParser.Of(BodyParser.Json.class)
	public static Result matchPeople() {
	  JsonNode json = request().body().asJson();
	  /*String name = json.findPath("name").getTextValue();
	  if(name == null) {
	    return badRequest("Missing parameter [name]");
	  } else {
	    return ok("Hello " + name);
	  }*/
	  ArrayList<NormalisedRecord> transformedRecords = new ArrayList<NormalisedRecord>();
	  Map<String, Integer> weightings = new HashMap<String, Integer>();
	  if(!transformRequest(json, transformedRecords, weightings))
	  {
	  	  return badRequest("The submitted Json was not valid.");
	  }
	  NamesDisambiguator disambiguator = new NamesDisambiguator();
	  disambiguator.matchCollection(transformedRecords);

	  return ok(jsonResults(transformedRecords));
	}

    public static Result moreInfo(){
    	return ok(more_info.render());
    }

    private static boolean transformRequest(JsonNode submissions, ArrayList<NormalisedRecord> transformedRecords, Map<String, Integer> weightings){
    	JsonNode matchRequest;
    	if((matchRequest = submissions.findPath("match_request")) == null)
    		return false;
    	JsonNode weightingNode = matchRequest.findPath("weightings");
    	JsonNode individualsNode = matchRequest.findPath("individuals");
    	for (JsonNode individualNode : individualsNode) {
    		NormalisedRecord n = new NormalisedRecord();
    		if((individualNode.findPath("submission_id")) == null)
    			continue;

    		NormalisedIdentifier submissionIdentifier = new NormalisedIdentifier();
			submissionIdentifier.setIdentifier(individualNode.findPath("submission_id").getTextValue());
			submissionIdentifier.setBasisFor("SUBMISSION ID");
			submissionIdentifier.setSourceID(individualNode.findPath("submission_id").getTextValue());
			submissionIdentifier.setSourceName("SUBMISSION");
			submissionIdentifier.setSourceURL("SUBMISSION");
			n.addNormalisedIdentifier(submissionIdentifier);

    		JsonNode namesNode;
    		if((namesNode = individualNode.findPath("names")) != null){
    			for(JsonNode nameNode: namesNode){
    				String personNameChars = nameNode.getTextValue().trim();
					NormalisedName newName = new NormalisedName();
					newName.setName(personNameChars);
					newName.setSourceID(submissionIdentifier.getIdentifier());
					newName.setSourceName(submissionIdentifier.getSourceName());
					newName.setSourceURL(submissionIdentifier.getSourceURL());
					PersonNameParser nameParser = new PersonNameParser();
					nameParser.parse(newName.getName());
					n.getNormalisedTitles().addAll(nameParser.getTitles());
					newName.getFamilyNames().addAll(nameParser.getFamilyNameComponents());
					newName.getGivenNames().addAll(nameParser.getGivenNameComponents());
					n.addNormalisedName(newName);
    			}
    		}

			JsonNode fieldsOfActivityNode;
			if((fieldsOfActivityNode = individualNode.findPath("fields_of_activity")) != null){
    			for(JsonNode fieldOfActivityNode: fieldsOfActivityNode){
    				String fieldOfActivityChars = fieldOfActivityNode.getTextValue().trim();
					NormalisedFieldOfActivity newFieldOfActivity = new NormalisedFieldOfActivity();
					newFieldOfActivity.setFieldOfActivity(fieldOfActivityChars);
	  	            newFieldOfActivity.setSourceID(submissionIdentifier.getIdentifier());
	  	            newFieldOfActivity.setSourceName(submissionIdentifier.getSourceName());
	  	           	newFieldOfActivity.setSourceURL(submissionIdentifier.getSourceURL());
	  	           	n.addNormalisedFieldOfActivity(newFieldOfActivity);
    			}
    		}

    		JsonNode identifiersNode;
			if((identifiersNode = individualNode.findPath("identifiers")) != null){
    			for(JsonNode identifierNode: identifiersNode){
    				String identifier = identifierNode.findPath("identifier").getTextValue();
    				String basisFor = identifierNode.findPath("basis_for").getTextValue();
					if(identifier != null && basisFor != null){
						NormalisedIdentifier newIdentifier = new NormalisedIdentifier();
						newIdentifier.setIdentifier(identifier);
						newIdentifier.setBasisFor(basisFor);
	  	            	newIdentifier.setSourceID(submissionIdentifier.getIdentifier());
	  	            	newIdentifier.setSourceName(submissionIdentifier.getSourceName());
	  	           		newIdentifier.setSourceURL(submissionIdentifier.getSourceURL());
						n.addNormalisedIdentifier(newIdentifier);

					}
					
					
	  	          
    			}
    		}


    		JsonNode affiliationsNode;
			if((affiliationsNode = individualNode.findPath("affiliations")) != null){
    			for(JsonNode affiliationNode: affiliationsNode){
    				String affiliationName = affiliationNode.getTextValue().trim();
    					
						NormalisedAffiliation newAffiliation = new NormalisedAffiliation();
						newAffiliation.setAffiliationName(affiliationName);
	  	            	newAffiliation.setSourceID(submissionIdentifier.getIdentifier());
	  	            	newAffiliation.setSourceName(submissionIdentifier.getSourceName());
	  	           		newAffiliation.setSourceURL(submissionIdentifier.getSourceURL());
	  	           		
	  	           		NormalisedRecord affiliationRecord = new NormalisedRecord();
	  	           		affiliationRecord.setType(NormalisedRecord.INSTITUTION);
	  	           		
	  	           			NormalisedName affiliationRecordName = new NormalisedName();
	  	           			affiliationRecordName.setName(newAffiliation.getAffiliationName());
							
							CorporateBodyNameParser cp = new CorporateBodyNameParser();
							cp.parse(newAffiliation.getAffiliationName());
							affiliationRecordName.getGivenNames().addAll(cp.getNameComponents());
						
						affiliationRecord.getNormalisedNames().add(affiliationRecordName);
						

	  	           		newAffiliation.setAffiliation(affiliationRecord);
						n.addNormalisedAffiliation(newAffiliation);

					
					
					
	  	          
    			}
    		}


    		JsonNode resultPublicationsNode;
  			if((resultPublicationsNode = individualNode.findPath("result_publications")) != null){
    			for(JsonNode resultPublicationNode: resultPublicationsNode){
    				String title = resultPublicationNode.findPath("title").getTextValue();
    				
					if(title != null){
						NormalisedResultPublication newResultPublication = new NormalisedResultPublication();
						newResultPublication.setTitle(title);
	  	            	newResultPublication.setSourceID(submissionIdentifier.getIdentifier());
	  	            	newResultPublication.setSourceName(submissionIdentifier.getSourceName());
	  	           		newResultPublication.setSourceURL(submissionIdentifier.getSourceURL());
						n.addNormalisedResultPublication(newResultPublication);

					}
					
					
	  	          
    			}
    		}


    		transformedRecords.add(n);
    	}

    	return true;
    }

    private static ObjectNode jsonResults(ArrayList<NormalisedRecord> matchedRecords){
    	ObjectNode results = Json.newObject();
    	for(NormalisedRecord record: matchedRecords){
    		   ObjectNode result = Json.newObject();
    		   Iterator it = record.getMatchResults().entrySet().iterator();
			    while (it.hasNext()) {
			            Map.Entry pairs = (Map.Entry)it.next();
			            NormalisedRecord key = (NormalisedRecord) pairs.getKey();
			            Double value = (Double)pairs.getValue();
			 			result.put(key.getNormalisedIdentifiers().get(0).getIdentifier(), value);
			    }

	    	  
	    	  results.put(record.getNormalisedIdentifiers().get(0).getIdentifier(), result);
    	}
    	return results;
    }
  
}
