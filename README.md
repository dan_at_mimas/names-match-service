Names Match Service
===================

Author: Daniel Needham (daniel.needham@manchester.ac.uk)

Overview
--------

The Names Match Service is a RESTful web application built using the [Java Play Framework](http://www.playframework.com/) and providing the functionality to disambiguate individuals using the names disambiguation library.

The service accepts meta data pertaining to individuals in a Json encoded list form and then uses the names disambiguation library to derive match scores for each of these individuals. The match scores are then returned as a Json encoded array to be used in whatever way required.

The service is stateless ; it does not rely upon existing Names records to perform the matching, nor does it store any meta data submitted to it, nor the outcome scores. It is therefore expected that the client will maintain the state in whatever application they are using the service for.

More information about API usage, response and request formats is given in the application.

Setup
------------

The names match service is dependent upon:

1. Log4J
2. names-disambiguator

The play framework allows dependencies to be managed using the maven central repository, however the names-disambiguator [library](https://bitbucket.org/dan_at_mimas/names-disambiguator/downloads) will initially need to be manually added to the lib directory.

Applications built using the play framework can be deployed in a number of ways, refer to the [documentation](http://www.playframework.com/documentation/).


